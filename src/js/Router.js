/**
 * Created by Tushar Srivastava on 27/03/15.
 */
"use strict";
var TsRouter = function(config){
  this.config = config;
  this.init();
};

TsRouter.prototype.init = function(){
  this.request = {};
  this.routeMap = {};
  this.navOffMap = {};
  this.globalCallback = this.config.callback;
};

TsRouter.prototype.history = {};

TsRouter.prototype.getRequestObj = function (url) {
  this.origin = window.location.origin;
  if(this.request.currentUrl === undefined){
    this.request.initLoad = true;
    this.request.prevUrl = undefined;
    this.request.currentUrl = window.location.href.replace(this.origin, "");
  }else{
    this.request.initLoad = false;
    this.request.prevUrl = this.request.currentUrl;
    if(url === undefined) {
      this.request.currentUrl = window.location.href.replace(this.origin, "");
    }else{
      this.request.currentUrl = url;
    }
  }
  //this.request.currentUrl = window.location.pathname;
};
TsRouter.prototype.start = function(){
  var self = this;
  this.onUrlChange();
  document.addEventListener("click", function(event){
    var target = event.target || event.srcElement;
    if(target.getAttribute("href") !== undefined) {
      event.preventDefault();
      var url = target.getAttribute("href");
      self.getRequestObj(url);
      self.onUrlChange(url);
    }
  });
};

TsRouter.prototype.onUrlChange = function (url) {
  //TODO: Add old params before initializing request params object
  this.getRequestObj();
  this.request.prevParams = this.request.params;
  this.request.params = {};
  var currentLevel = this.resolvePath(url);
  if(currentLevel.valid === true){
    if(currentLevel.callback !== undefined && typeof currentLevel.callback === "Function"){
      currentLevel.callback(this.request);
    }
  }
};

TsRouter.prototype.addRoute = function (url, callback) {
  var currentLevel = this.getPath(url);
  currentLevel.valid = true;
  if(callback !== undefined) {
    currentLevel.callback = callback;
  }
};
TsRouter.prototype.parseParam = function(param){
  var str = param.split(/[\{\}]/);
  return str[1];
};
TsRouter.prototype.removeRoute = function(url) {
  var currentLevel = this.getPath(url);
  currentLevel.valid = false;
};
TsRouter.prototype.resolvePath = function(path) {
  var currentLevel = this.routeMap;
  this.request.params = {};
  if (path !== undefined) {
    if(path !== "/") {
      var pathSplitArray = path.split("/");
      var keyIndex = 0;
      for (var i = 0; i < pathSplitArray.length; i++) {
        var key = pathSplitArray[i];
        if (key !== "") {
          if (currentLevel.paramKey !== undefined && currentLevel.paramKey.length !== 0 && keyIndex !== currentLevel.paramKey.length ) {
            var currentParam = currentLevel.paramKey[keyIndex];
            this.request.params[currentParam] = key;
            keyIndex++;
            continue;
          }
          if (currentLevel.subRoutes === undefined) {
            return {
              valid: false
            };
          } else {
            if(currentLevel.subRoutes[key] === undefined){
              return {
                valid: false
              };
            }else {
              currentLevel = currentLevel.subRoutes[key];
            }
          }
        }
      }
    }
    return currentLevel;
  }
  return currentLevel;
};
TsRouter.prototype.getPath = function(path){
  var currentLevel = this.routeMap;
  if(path !== undefined) {
    if(path !== "/") {
      var pathSplitArray = path.split("/");
      for (var i = 0; i < pathSplitArray.length; i++) {
        var key = pathSplitArray[i];
        var paramKey = null;
        if (key !== "") {
          if (key.indexOf("{") === -1) {
            if (currentLevel.subRoutes !== undefined) {
              if (currentLevel.subRoutes[key] == undefined) {
                currentLevel.subRoutes[key] = {};
              }
              currentLevel = currentLevel.subRoutes[key];
            } else {
              currentLevel.subRoutes = {};
              currentLevel.subRoutes[key] = {};
              currentLevel = currentLevel.subRoutes[key];
            }
          } else {
            paramKey = this.parseParam(key);
          }
        }
        if (paramKey !== null) {
          if (currentLevel.paramKey !== undefined) {
            currentLevel.paramKey.push(paramKey);
          } else {
            currentLevel.paramKey = [paramKey];
          }
        }
      }
    }
    return currentLevel;
  }
};
TsRouter.prototype.errors = {};